//userActions.js
import axios from 'axios';
export function fetchUser() {
console.log('initiating fetch');
return function(dispatch) {
	dispatch({type: "FETCH_USER", payload: null});
  axios.get("http://rest.learncode.academy/api/test123/tweets")
    .then((response) => {
      dispatch({type: "FETCH_TWEETS_FULFILLED", payload: response.data})
    })
    .catch((err) => {
      dispatch({type: "FETCH_TWEETS_REJECTED", payload: err})
    })
};
}

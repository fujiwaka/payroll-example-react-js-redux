// file: AccountFields.jsx

import React from 'react';
import ReactDOM from 'react-dom';
export default class AccountFields extends React.Component {
  render () {
    return ( <div>
      <label>Name</label> 
      <input type="text"
             ref="name"
             defaultValue={ this.props.fieldValues.name } />

      <label>Password</label>
      <input type="password"
             ref="password"
             defaultValue={ this.props.fieldValues.password } />

      <label>Email</label>
      <input type="email"
             ref="email"
             defaultValue={ this.props.fieldValues.email } />

      <button onClick={ this.saveAndContinue.bind(this) }>Save and Continue</button></div>
    )
  }

  saveAndContinue (e) {
    e.preventDefault()
    console.log(this);
    // Get values via this.refs
    var data = {
      name     : ReactDOM.findDOMNode(this.refs.name).value,
      password :  ReactDOM.findDOMNode(this.refs.password).value,
      email    :  ReactDOM.findDOMNode(this.refs.email).value
    }

    this.props.saveValues(data)
    this.props.nextStep()
  }
}

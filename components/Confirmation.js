// file: SurveyFields.jsx

import React from 'react';
import ReactDOM from 'react-dom';
import Panel from 'react-bootstrap';
export default class SurveyFields extends React.Component {
  render () {
    return ( <div>
      <label>Success</label> 
      <button onClick={ this.previousStep.bind(this) }>Go back </button>
      <button onClick={ this.submit.bind(this) }>Finish </button>
    </div>

    )
  }
  previousStep() {
    this.props.previousStep()
  }
  submit() {
    this.props.submit();
  }
}

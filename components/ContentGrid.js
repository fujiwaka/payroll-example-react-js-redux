import React from "react";
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Clearfix from 'react-bootstrap/lib/Clearfix';

export default class ContentGrid extends React.Component {
  render() {
    const dummySentences = "'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.'";
    return (
      <Grid>
        <Row className="show-grid">
        {this.props.children.map(function(result) {
          console.log(result);
          return <Col key={result.key} sm={6} md={3}><code>&lt;{'Col sm={6} md={3}'} /&gt;</code> <br/> {result}</Col>;
        })}
        </Row>
      </Grid>
    );
  }
}

import React from "react";
import {connect} from 'react-redux';
import {fetchEmployees} from '../actions/employeesActions';
@connect(function(store) {
    return {
    employees: store.employees.employees
  }
})
export default class EmployeeCard extends React.Component {
  componentWillMount() {
      this.props.dispatch(fetchEmployees());
  }
  createMethod () {
    console.log('pressed create');
  }
  render() {
    var btnStyle =  {
         margin: '10px'
    };
    var thumbStyle = {
      margin: '20px'
    };
    const employees = this.props.employees;
    console.log(employees);
    const createMethod = this.createMethod;
    return (
      <div className="container" style={btnStyle}>
      <div className="row">
      {
          employees.map(function(employee) {
          return <div className="col-sm-6 col-md-3" key={employee.id}>
                <div className="caption text-center">
                  <div className="thumbnail">
                    <img src={employee.photo} className="img-rounded"/>
                      <h4>{employee.name}</h4>
                      <p>  <small> Position: {employee.job_role}</small> </p> 
                  </div>
                 </div>
               </div>
        })
      }
      </div>
        </div>
    );
  }
}

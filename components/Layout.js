//Layout.js
import React from 'react'
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';
import {fetchUser} from '../actions/userActions';
import {Button} from 'react-bootstrap';
import {Panel} from 'react-bootstrap';
import {Nav} from 'react-bootstrap';
import {NavItem} from 'react-bootstrap';
import {Navbar} from 'react-bootstrap';
import {Glyphicon} from 'react-bootstrap';
import {Clearfix} from 'react-bootstrap';
import {MenuItem} from 'react-bootstrap';
import EmployeeCard from './EmployeeCard';
import ContentGrid from './ContentGrid';
import Registration from './Registration';
import {IndexLink} from 'react-router';

@connect(function(store) {
    return {
		user: store.user
	}
})


export default class Layout extends React.Component {
	render() {
	var fontAwesomeStyle = {
		paddingLeft: "30%"
	};
	var navTextStyle= {
		fontSize: "13px"
	};
	var navItemStyle = {
		
	};
		return (
			<div id="wrapper">

        <div id="sidebar-wrapper">
            <ul className="sidebar-nav">
                <li className="sidebar-brand">
                    <a href="#">
                        Start Bootstrap
                    </a>
                </li>
                <li>
                    <a href="#">Dashboard</a>
                </li>
                <li>
                    <a href="#">Shortcuts</a>
                </li>
                <li>
                    <a href="#">Overview</a>
                </li>
                <li>
                    <a href="#">Events</a>
                </li>
                <li>
                    <a href="#">About</a>
                </li>
                <li>
                    <a href="#">Services</a>
                </li>
                <li>
                    <a href="#">Contact</a>
                </li>
            </ul>
        </div>

        <div id="page-content-wrapper">
            <div className="container-fluid">
               <div className="row">
			    <Navbar className="navbar-custom">
					<Nav bsStyle="pills" pullRight={true}>
					    <NavItem eventKey={1} href="/logout"><i className="fa fa-cog" aria-hidden="true"> </i>
					    </NavItem>
				    </Nav>
			   </Navbar>
			    {this.props.children}
		   </div>
            </div>
        </div>

    </div>
  
	   );
	}
}
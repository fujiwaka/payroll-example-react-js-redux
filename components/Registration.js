import React from 'react';
import ReactDOM from 'react-dom';
import AccountFields from './AccountFields';
import SurveyFields from './SurveyFields';
import Confirmation from './Confirmation';
import Layout from './Layout';
var fieldValues = {
  name     : null,
  email    : null,
  password : null,
  age      : null,
  colors   : []
};
export default class Registration extends React.Component {
	constructor () {
		super();
			this.state =  {
				step: 1
			};
		}
	render () {
	  switch (this.state.step) {
	    case 1:
	      return <AccountFields fieldValues={fieldValues}
	                            nextStep={this.nextStep.bind(this)}
	                            saveValues={this.saveValues.bind(this)} />
	    case 2:
	      return <SurveyFields fieldValues={fieldValues}
	                           nextStep={this.nextStep.bind(this)}
	                           previousStep={this.previousStep.bind(this)}
	                           saveValues={this.saveValues.bind(this)} />
	    case 3:
	      return <Confirmation fieldValues={fieldValues}
	                           previousStep={this.previousStep.bind(this)}
	                           submit={this.submit.bind(this)}/>
	    case 4:
	      return <Success fieldValues={fieldValues} />
	  }
	}
	saveValues (fields) {
		console.log('saving value');
    // Remember, `fieldValues` is set at the top of this file, we are simply appending
    // to and overriding keys in `fieldValues` with the `fields` with Object.assign
    // See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
    fieldValues = Object.assign({}, fieldValues, fields)
  
	}

	nextStep () {
	  this.setState({
	    step : this.state.step + 1
	  })
	}

	// Same as nextStep, but decrementing
	previousStep () {
	  this.setState({
	    step : this.state.step - 1
	  })
	}
	submit() {
		this.props.cleanup(<Layout/>);
	}
	}
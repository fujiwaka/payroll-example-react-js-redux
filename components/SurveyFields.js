// file: SurveyFields.jsx

import React from 'react';
import ReactDOM from 'react-dom';
export default class SurveyFields extends React.Component {
  render () {
    return ( <div>
      <label>Thoughts</label> 
      <input type="text-area"
             ref="age"
             defaultValue={ this.props.fieldValues.age }/>

      <button onClick={ this.saveAndContinue.bind(this) }>Save and Continue</button>
      <button onClick={ this.previousStep.bind(this) }>Go back </button>
    </div>

    )
  }
  previousStep() {
    this.props.previousStep()
  }
  saveAndContinue (e) {
    e.preventDefault()
    console.log(this);
    // Get values via this.refs
    var data = {
      age     : ReactDOM.findDOMNode(this.refs.age).value
    }
    console.log('got here');
    this.props.saveValues(data)
    this.props.nextStep()
  }
}

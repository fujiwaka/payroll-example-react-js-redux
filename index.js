import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Layout from './components/Layout';
import store from './store';
import {Router,Route, Link, hashHistory,IndexRoute} from 'react-router';
import EmployeeCard from './components/EmployeeCard';
console.log('index.js initiated');
var app = document.getElementById('App');

ReactDOM.render(
	<Provider store={store}> 
		<Router history={hashHistory}> 
				<Route path='/' component={Layout}>
					<Route path="settings" component={EmployeeCard}> </Route>
				 </Route>
		</Router>
	</Provider>
,app);
//reducer (index.js so that webpack takes it as defaut file to look at)

import { combineReducers } from "redux"
import user from "./userReducer"
import employees from './employeesReducer';
export default combineReducers({
    user,
    employees
});

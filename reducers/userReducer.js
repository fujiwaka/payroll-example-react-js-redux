
export default function reducer (state = {
		tweets: [],
		fetching: false,
		fetched: false,
		error: null
		},action) {
		switch (action.type) {
		case "FETCH_USER": {
			return {...state, fetching: true, fetched: false}
		}
		case "FETCH_TWEETS_FULFILLED": {
			return {...state, fetching: false, tweets: action.payload}

		}
		default:
		return state;
	}

}

module.exports = {
  entry: './',
  output: {
    path: __dirname+'/js',
    filename: "bundle.js",
    publicPath: "/static/"
  },

  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: "babel",
      include: __dirname,
      query: {
        presets: [ "es2015", "react", "react-hmre", 'stage-2'],
        plugins: ['transform-decorators-legacy','transform-class-properties']
      }
    }]
  }
}
